FROM node:latest

RUN mkdir /usr/src/app

WORKDIR /usr/src/app/vue-docker

RUN npm install -g @vue/cli

COPY . .